var redIcon = L.icon({
    iconUrl: 'img/icons/location_red_small.png',
    iconSize: [38,55],
    iconAnchor: [19, 54],
    popupAnchor: [0, -55]
});


var app = {
    // Application Constructor
    initialize: function() {
        this.map = null;
        this.popups = {};
        this.libraries = {};
        this.selectedLibrary = null;


        this.bindEvents();

        this.initMap();

        this.addButton('Preference', '#e77b61', 'preference_red', 'preference_white');
        this.addButton('Nearby', '#eccb57', 'nearby_yellow', 'nearby_white');
        this.addButton('Confirm', '#1dbaa7', 'confirm_green', 'confirm_white');
        this.addButton('Cancel', '#7ad0e9', 'cancel_blue', 'cancel_white');

        this.addLibrary('Grainger', '8:30am - 12:00am', 
            '1301 W Springfield Ave, Urbana, IL 61801', [40.103078,-88.2228102]);  

        this.popups['Grainger'].openPopup();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    },
    initMap: function() {
        this.map = L.map('map', {zoomControl: false}).setView([40.103078,-88.2228102], 16);

        L.tileLayer.grayscale('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);    
    },
    addButton: function(text, color, icon_color_name, icon_white_name, onclick) {
        var buttons = document.getElementById('buttons');
        var button = document.createElement('div');
        button.className = 'button';

        var buttonHighlight = document.createElement('div');
        buttonHighlight.className = 'buttonHighlight';
        buttonHighlight.style.background = color;

        var buttonBody = document.createElement('div');
        buttonBody.className = 'buttonBody';

        var buttonText = document.createElement('div');
        buttonText.textContent = text;
        buttonText.className = 'buttonText';

        var buttonImage = document.createElement('img');
        buttonImage.src = 'img/icons/'+icon_color_name+'.png';
        buttonImage.className = 'buttonImage';

        buttonBody.appendChild(buttonImage);
        buttonBody.appendChild(buttonText);
        button.appendChild(buttonHighlight);
        button.appendChild(buttonBody);

        buttons.appendChild(button);

        buttonBody.addEventListener('touchstart', function() {
            buttonBody.style.background = color;
            buttonImage.src = 'img/icons/'+icon_white_name+'.png';
            buttonText.style.color = 'rgb(247, 248, 252)';
            if(onclick) {
                onclick();
            }
        });
        buttonBody.addEventListener('touchend', function() {
            buttonText.style.color = 'black';
            buttonImage.src = 'img/icons/'+icon_color_name+'.png';
            buttonBody.style.background = "rgb(247, 248, 252)";
        });
    },
    hideAllPopups: function() {
        for(var x in this.popups) {
            this.popups[x].closePopup();
        }
    },
    addLibrary: function(name, hours, address, longLat) {
        var marker = L.marker(longLat, {icon: redIcon}).addTo(this.map);
        var popup = marker.bindPopup("<div style='font-size:1.5em;font-weight:700;'>"+name+"</div>"+hours+"<br>"+address+"<br>");
        this.popups[name] = popup;
        var that = this;
        marker.on('click', function() {
            app.hideAllPopups();
            app.selectLibrary = name;
            popup.openPopup();
        });

        var library = new Library();
    },

};

app.initialize();